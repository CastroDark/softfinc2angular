import { Component, OnInit } from "@angular/core";
import * as jsPDF from "jspdf";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      this.printDemo();
    }, 1000);
  }

  printDemo() {
    // Default export is a4 paper, portrait, using milimeters for units
    // var doc = new jsPDF();

    var doc = new jsPDF("p", "pt", "a4", true);

    //doc.setTextColor(0, 85, 136);

    // doc.setFontSize(20);
    // doc.setFont("helvetica");
    // doc.setFontType("bold");

    //doc.text("Hello world!", 10, 20); //init a4
    //doc.text("Hello world!", 10, 840); //end a4
    // doc.save("a4.pdf");

    doc = this.headTitleCompany(doc);

    var blob = doc.output("blob");
    window.open(URL.createObjectURL(blob));
  }

  headTitleCompany(doc: jsPDF): string {
    var doc = new jsPDF("p", "mm", "a4");

    doc.setFont("courier");
    doc.setFontType("bold");
    doc.setFontSize(19);

    //var to insert
    var textPrint = "";
    let multNextLIne: number = 0;
    let nextBr = 0;
    let separatorLine: number = 0;
    let maxLenth = 0;
    let numberSecLine = 0;
    let primaryLine = 20;

    let lineSalt = 0;

    //name company
    textPrint = "Finca Negro Vargas";
    maxLenth = 40;
    doc = this.centerText(doc, textPrint, maxLenth, primaryLine, 200);
    multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);
    lineSalt = multNextLIne;
    nextBr = 4 * lineSalt;
    separatorLine = multNextLIne == 0 ? nextBr : nextBr + primaryLine;

    //Slogan Company
    doc.setFontSize(10);
    textPrint = "Bananos Organicossd  asdsad a   a... ";
    maxLenth = 40;
    doc = this.centerText(doc, textPrint, maxLenth, separatorLine, 200);
    multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);

    lineSalt += multNextLIne;
    nextBr = 4 * lineSalt;
    separatorLine = multNextLIne == 0 ? nextBr : nextBr + primaryLine;

    doc.setFontType("normal");
    textPrint = "Calle de Ejmplo Nro x ";
    maxLenth = 40;
    doc = this.centerText(doc, textPrint, maxLenth, separatorLine, 200);

    //multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);
    // nextBr = nextBr++ * numberSecLine;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr * multNextLIne;

    // //Adrress Company
    // doc.setFontSize(9);
    // doc.setFontType("normal");
    // textPrint = "Direccion de Empresa x sa sadsadsadsad asdsadsad   ";
    // maxLenth = 40;
    // numberSecLine++;
    // doc = this.centerText(
    //   doc,
    //   textPrint,
    //   maxLenth,
    //   primaryLine + separatorLine,
    //   200
    // );

    // multNextLIne = this.calculateLine(textPrint, maxLenth);
    // nextBr = 5 * numberSecLine;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr * multNextLIne;

    return doc;
  }

  centerText(
    doc: jsPDF,
    text: string,
    maxLength: number,
    topPosition: number,
    pdfWidth: number
  ) {
    let lineTop: number = 500;
    // var lMargin = 15; //left margin in mm
    // var rMargin = 15; //right margin in mm
    //pdfInMM = 210; // width of A4 in mm
    var pageCenter = pdfWidth / 2;

    //var doc = new jsPDF("p", "mm", "a4");
    // var paragraph = "";

    let countLength = 0;
    let newText: string = "";
    for (let i = 0; i < text.length; i++) {
      //console.log(text.charAt(i));
      newText += text.charAt(i);
      if (countLength == maxLength) {
        newText += "\n\n";
        countLength = 0;
      }
      countLength++;
    }
    //console.log(newText);

    // var lines = doc.splitTextToSize(newText, pdfInMM - lMargin - rMargin);

    var lines = newText.split("\n\n");
    var dim = doc.getTextDimensions("Text");
    var lineHeight = dim.h;

    for (var i = 0; i < lines.length; i++) {
      lineTop = (lineHeight / 1.5) * i;
      doc.text(lines[i], pageCenter, topPosition + lineTop, "center"); //see this line
    }
    return doc;
  }

  calculateLine(text: string, maxWidth: number, nexBr: number) {
    // let lines= text.split()
    let countLength = 0;

    let newText: string = "";
    for (let i = 0; i < text.length; i++) {
      //console.log(text.charAt(i));
      newText += text.charAt(i);
      if (countLength > maxWidth) {
        newText += "\n\n";
        countLength = 0;
      }
      countLength++;
    }

    let longitud: any[];
    longitud = newText.split("\n\n");
    if (longitud.length == 0) {
      return 1;
    } else {
      return longitud.length;
    }
  }
}
